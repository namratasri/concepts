package com.nam.concepts.exceptions.unchecked;


public class UncheckedException extends RuntimeException {

    public UncheckedException(String message) {

        super(message);
    }

}
