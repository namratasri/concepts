package com.nam.concepts.exceptions.unchecked;

public class Executor {

    public static void main(String[] args) {
        try {
            System.out.println("executing try");
            //thisMethodThrowsException();
            throw new UncheckedExceptionChild("child Excepton 1");
        } catch (UncheckedExceptionChild e) {
            System.out.println("I catched Child Exception " + e.getMessage());
        } catch (UncheckedException e) {
            System.out.println("I catched UncheckedException " + e.getMessage());
        } finally {
            System.out.println("finally : this peice of code will get executed no matter what happens...");
        }
    }

    public static void thisMethodThrowsException() throws UncheckedException {
        int a = 0;
        int b = 2;
        if (a != 0) {
            int i = b / a;
        }
    }

}