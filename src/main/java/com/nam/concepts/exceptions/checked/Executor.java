package com.nam.concepts.exceptions.checked;

import java.sql.SQLException;

public class Executor {

    public static void main(String[] args)  {
        try {
            thisMethodThrowsException();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void thisMethodThrowsException() throws Exception {
        throw new CheckedException("this is : CheckedException");
    }

}