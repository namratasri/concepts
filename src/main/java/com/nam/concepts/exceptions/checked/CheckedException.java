package com.nam.concepts.exceptions.checked;


public class CheckedException extends Exception {

    public CheckedException(String message) {
        super(message);
    }

}
