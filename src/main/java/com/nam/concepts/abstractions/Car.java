package com.nam.concepts.abstractions;


public abstract class Car extends Vehicle {

    @Override
    boolean wheels() {
        System.out.println("Car has 4 wheels--");
        return false;
    }
}

class Car1 extends Car {
    @Override
    boolean colour() {
        System.out.println("Car is Black--");
        return false;
    }


    boolean colour(String color) {
        System.out.println("Car is ---" + color);
        return false;
    }

    @Override
    boolean wheels() {
        System.out.println("Car1 has 4 wheels --");
        return false;
    }
}


class Car2 extends Car1 {
    boolean colour(String color) {
        System.out.println("Car2 is --->" + color);
        return false;
    }
}

class main {
    public static void main(String args[]) {
        Car1 c1 = new Car2();
        System.out.println(c1.colour());
        System.out.println(c1.colour("blue"));
    }
}