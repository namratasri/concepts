package com.nam.concepts.abstractions;
public abstract class Vehicle {
    abstract boolean wheels();
    abstract boolean colour();
}

